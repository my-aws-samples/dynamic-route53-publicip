# dynamic-route53-publicip

## OverView

1. EC2起動時、PublicIPをR53の指定したZonneにAレコードをUpsert.
2. EC2停止時、AレコードをDelete

## Usage

- EC2のタグ、DnsNameにサブドメインを指定

## Requirement

- R53のHostedZoneがあること

## Deploy

```bash
sam deploy --guided
Stack Name [dynamic-route53-publicip]: 
AWS Region [ap-northeast-1]: 
Parameter DomainName [Input_Your_DomainName]: Your Domain Name
#Shows you resources changes to be deployed and require a 'Y' to initiate deploy
Confirm changes before deploy [Y/n]: 
#SAM needs permission to be able to create roles to connect to the resources in your template
Allow SAM CLI IAM role creation [Y/n]: 
Save arguments to samconfig.toml [Y/n]: 
```

## Develop Environment

- aws cloud9
- Python 3.8
- sam cli, version 0.44.0

## Created AWS Resources

- CloudFormation Stack: dynamic-route53-publicip
- CloudWatchEvent: setRecordTrigger
- IAMRole: setRecordRole
- CloudWatchLogs: setRecordLogGroup
- Lambda Function: setRecord
- CloudWatchEvent: delRecordTrigger
- IAMRole: delRecordRole
- CloudWatchLogs: delRecordLogGroup
- Lambda Function: delRecord