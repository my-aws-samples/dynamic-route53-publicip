import json
import boto3
import os
import logging
from botocore.exceptions import ClientError

ec2 = boto3.client('ec2')
r53 = boto3.client('route53')
hosted_zone = os.getenv('DOMAIN_NAME')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
  instance_id = event['detail']['instance-id']
  print(instance_id)
  
  response = ec2.describe_instances(
    InstanceIds = [instance_id]
  )['Reservations'][0]['Instances'][0]
  
  public_ip = response['PublicIpAddress']
  tag = [t['Value'] for t in response['Tags'] if t['Key'] == 'DnsName']
  
  hosted_zone_id = r53.list_hosted_zones_by_name(
    DNSName = hosted_zone + '.'
  )['HostedZones'][0]['Id']
  hosted_zone_id = hosted_zone_id.split('/')[-1]
  print(tag[0] + '.' + hosted_zone)
  
  try:
    response = r53.change_resource_record_sets(
      HostedZoneId = hosted_zone_id,
      ChangeBatch = {
        'Changes': [
          {
            'Action': 'DELETE',
            'ResourceRecordSet': {
              'Name': tag[0] + '.' + hosted_zone + '.',
              'Type': 'A',
              'TTL': 60,
              'ResourceRecords': [{'Value': public_ip}]
            }
          }
        ]
      }
    )
  except Exception as e:
    logging.error('type : %s', type(e))
  else:
    logging.info(response)
